package com.road.design.mode.factory.abstractFactory;

/**
 * 颜色接口
 */
public interface Color {
	void fill();
}
