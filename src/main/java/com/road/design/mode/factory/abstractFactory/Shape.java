package com.road.design.mode.factory.abstractFactory;

/**
 * x形状接口
 */
public interface Shape {
	void draw();
}
