package com.road.design.mode.factory.factoryMode;

public interface Shape {
	void draw();
}
