package com.road.design.mode.builder;

public interface Packing {
	public String pack();
}
