package com.road.data.structure;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: girl
 * @description: 无向图
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2021-12-30 14:33
 **/
public class Graph {
    boolean found = false;

    private int v; // 顶点的个数
    // 邻接表来存储图
    private LinkedList<Integer> adj[]; // 邻接表

    public Graph(int v) {
        this.v = v;
        adj = new LinkedList[v];
        for (int i = 0; i < v; ++i) {
            adj[i] = new LinkedList<>();
        }
    }

    public void addEdge(int s, int t) { // 无向图一条边存两次
        adj[s].add(t);
        adj[t].add(s);
    }

    /**
     * 广度优先搜索（Breadth-First-Search）
     * <p>
     * bfs() 函数就是基于之前定义的，图的广度优先搜索的代码实现。
     * <p>
     * 其中 s 表示起始顶点，t 表示终止顶点。我们搜索一条从 s 到 t 的路径。
     * <p>
     * 实际上，这样求得的路径就是从 s 到 t 的最短路径。
     *
     * @param s 表示起始顶点
     * @param t 表示终止顶点
     */
    public void bfs(int s, int t) {
        if (s == t) return;
        // 用来标记已经被记录过的顶点
        boolean[] visited = new boolean[v];
        visited[s] = true;
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);

        // 用来记录搜索路径
        int[] prev = new int[v];
        for (int i = 0; i < v; ++i) {
            prev[i] = -1;
        }

        while (queue.size() != 0) {
            // 取出Queue中LinkedList数组中的其中一个，LinkedList数组中存在与该顶点有关联的其他顶点
            int w = queue.poll();
            // 遍历当前顶点中链接的其他顶点
            for (int i = 0; i < adj[w].size(); ++i) {
                //
                int q = adj[w].get(i);
                if (!visited[q]) {
                    prev[q] = w;
                    if (q == t) {
                        print(prev, s, t);
                        return;
                    }
                    visited[q] = true;
                    queue.add(q);
                }
            }
        }
    }

    /**
     * 深度优先搜索（Depth-First-Search)
     *
     *
     * @param s
     * @param t
     */
    public void dfs(int s, int t) {
        boolean[] visited = new boolean[v];
        int[] prev = new int[v];
        for (int i = 0; i < v; i++) {
            prev[i] = -1;
        }
        recurDfs(s, t, visited, prev);
        print(prev, s, t);

    }

    private void recurDfs(int w, int t, boolean[] visited, int[] prev) {
        if (found == true) {
            return;
        }
        visited[w] = true;
        if (w == t) {
            found = true;
            return;
        }
        for (int i = 0; i < adj[w].size(); i++) {
            int q = adj[w].get(i);
            if (!visited[q]) {
                prev[q] = w;
                recurDfs(q, t, visited, prev);
            }
        }
    }

    private void print(int[] prev, int s, int t) { // 递归打印s->t的路径
        if (prev[t] != -1 && t != s) {
            print(prev, s, prev[t]);
        }
        System.out.print(t + " ");
    }

}
