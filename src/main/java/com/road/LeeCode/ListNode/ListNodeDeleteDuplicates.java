package com.road.LeeCode.ListNode;

/**
 * @author malulu
 * @desc 删除排序链表中的重复元素
 * @date 2021/2/22
 */
public class ListNodeDeleteDuplicates {

    /**
     * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
     *
     * 示例 1:
     *
     * 输入: 1->1->2 输出: 1->2 示例 2:
     *
     * 输入: 1->1->2->3->3 输出: 1->2->3
     *
     * @return
     */
    public ListNode deleteDuplicates(ListNode head) {
        ListNode current = head;
        while (current != null && current.next != null) {
            // 比较当前节点的与当前节点下个节点值是否一样，若一样则将下个指针指向下下个节点
            if (current.next.val == current.val) {
                current.next = current.next.next;

            } else {
                current = current.next;
            }
        }
        return head;
    }
}
