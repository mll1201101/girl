package com.road.LeeCode.treenode;

/**
 * @program: girl
 * @description: 二叉树实体
 * @packagename: com.road.LeeCode.treenode
 * @author: Road
 * @date: 2022-01-07 14:44
 **/
public class BinaryTree {
    public int val;
    public BinaryTree left;
    public BinaryTree right;
    public BinaryTree next;

    public BinaryTree() {
    }

    public BinaryTree(int val) {
        val = val;
    }

    public BinaryTree(int val, BinaryTree left, BinaryTree right, BinaryTree next) {
        val = val;
        left = left;
        right = right;
        next = next;
    }
    
}
