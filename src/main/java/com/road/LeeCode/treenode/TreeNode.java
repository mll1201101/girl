package com.road.LeeCode.treenode;

/**
 * @program: girl
 * @description: 二叉树实体
 * @packagename: com.road.LeeCode.treenode
 * @author: Road
 * @date: 2022-01-06 17:00
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;

    public TreeNode() {
    }

    public TreeNode(int val) {
        this.val = val;
    }

    public TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}
