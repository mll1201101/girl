package com.road.LeeCode;

/**
 * @program: girl
 * @description:
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-02-15 15:37
 **/
public class RobBase {

    /**
     * 打家劫舍2
     * <p>
     * 动态规划写法一
     * <p>
     * 四次遍历，四个数组
     *
     * @param nums
     * @return
     */
    public int robOne(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return 0;
        }
        if (length == 1) {
            return nums[0];
        }
        // 环形看成直线，去掉第一个元素，或者去掉最后一个元素
        // 场景一，去掉最后一个元素
        int[] dp1 = new int[length - 1];
        for (int i = 0; i < length - 1; i++) {
            dp1[i] = nums[i];
        }

        // 场景二，去掉第一个元素
        int[] dp2 = new int[length - 1];
        for (int i = 0; i < length - 1; i++) {
            dp2[i] = nums[i + 1];
        }
        return Math.max(inRob(dp1), inRob(dp2));
    }

    private int inRob(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return 0;
        }
        if (length == 1) {
            return nums[0];
        }

        int[] dp = new int[length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < nums.length; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }
        return dp[length - 1];
    }

    public int robTwo(int[] nums) {
        int length = nums.length;
        if (length == 0) {
            return 0;
        }
        if (length == 1) {
            return nums[0];
        }
        // 当环形元素只有两个，取最大值
        if (length == 2) {
            return Math.max(nums[0], nums[1]);
        }

        //环形的看成直线的话，可以理解成：去掉最后一个元素，和去掉第一个元素两种场景；然后找到这两种场景的最大值。
        //场景一：去掉最后一个元素
        int[] dp = new int[length - 1];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < length - 1; i++) {
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i]);
        }
        // 此处的length - 1 - 1是 dp.length - 1 的意思
        int max1 = dp[length - 1 - 1];

        // 场景二：去掉第一个元素，共用场景一的dp数组，重新赋值
        dp[0] = nums[1];
        dp[1] = Math.max(nums[1], nums[2]);
        for (int i = 2; i < length - 1; i++) {//此处的length - 1是dp.length
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + nums[i + 1]);
        }
        // 此处的length - 1 - 1是 dp.length - 1 的意思
        int max2 = dp[length - 1 - 1];

        return Math.max(max1, max2);
    }


    /**
     * 打家劫舍
     *
     * @param nums
     * @return
     */
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        int length = nums.length;
        if (length == 1) {
            return nums[0];
        }
        //
        int[] dp = new int[length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0], nums[1]);
        for (int i = 2; i < length; i++) {
            dp[i] = Math.max(dp[i - 2] + nums[i], dp[i - 1]);
        }
        return dp[length - 1];
    }

    public int robRing(int[] nums) {
        int length = nums.length;
        if (length == 1) {
            return nums[0];
        } else if (length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        return Math.max(robRange(nums, 0, length - 2), robRange(nums, 1, length - 1));
    }

    public int robRange(int[] nums, int start, int end) {
        int first = nums[start], second = Math.max(nums[start], nums[start + 1]);
        for (int i = start + 2; i <= end; i++) {
            int temp = second;
            second = Math.max(first + nums[i], second);
            first = temp;
        }
        return second;
    }

    /**
     * @param items 商品价格
     * @param n     商品个数
     * @param w     表示满减条件
     */
    public static void doubleAdvance(int[] items, int n, int w) {
        boolean[][] states = new boolean[n][3 * w + 1];
        states[0][0] = true;
        //
        if (items[0] <= 3 * w) {
            states[0][items[0]] = true;
        }

        for (int i = 1; i < n; i++) {
            for (int j = 0; j <= 3 * w; j++) {
                if (states[i - 1][j] = true) {
                    states[i][j] = true;
                }
            }

            for (int j = 0; j <= 3 * w; j++) {
                if (states[i - 1][j] == true) {
                    states[i][j + items[i]] = true;
                }
            }
        }
    }
}
