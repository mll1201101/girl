package com.road.LeeCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: girl
 * @description: 斐波那契数列
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-01-18 11:10
 **/
public class FibonacciSequence {

    /**
     * 递归调用
     *
     * @param n
     * @return
     */
    public int fib(int n) {
        if (n < 2) {
            return n;
        }
        return fib(n - 1) + fib(n - 2);
    }

    public int fibFor(int n) {
        Map<Integer, Integer> memo = new HashMap<>(n + 1);
        return helper(memo, n);
    }

    public int helper(Map<Integer, Integer> memo, int n) {
        if (n < 2) {
            return n;
        }
        if (memo.get(n) != null) {
            return memo.get(n);
        }

        int memoN = helper(memo, n - 1) + helper(memo, n - 2);
        System.out.println("memoN" + memoN);
        memo.put(memoN, memoN);
        return memo.get(memoN);
    }

    public static void main(String[] args) {
        new FibonacciSequence().fibFor(4);
    }

    // 表示queen存储在哪一列
    int[] result = new int[8];

    int maxW = Integer.MIN_VALUE;

    // 调用方式：cal8queens(0)
    public void cal8queens(int row) {
        if (row == 8) {
            printQueen(result);
            return;
        }
        for (int column = 0; column < 8; column++) {
            if (isOk(row, column)) {
                result[row] = column;
                cal8queens(row + 1);
            }
        }
    }

    /**
     * 判断row行column列放置是否合适
     *
     * @param row
     * @param column
     * @return
     */
    private boolean isOk(int row, int column) {
        int leftup = column - 1, rightup = column + 1;
        for (int i = row - 1; i >= 0; --i) {
            if (result[i] == column) return false;
            if (leftup >= 0) {
                if (result[i] == leftup) return false;
            }

            if (rightup < 8) {
                if (result[i] == rightup) return false;
            }
            --leftup;
            ++rightup;
        }
        return true;
    }

    private void printQueen(int[] result) {
        for (int row = 0; row < 8; ++row) {
            for (int column = 0; column < 8; ++column) {
                if (result[row] == column) {
                    System.out.println("Q ");
                } else {
                    System.out.println("* ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }


    /**
     * weight:物品重量，n:物品个数，w:背包可承载重量
     *
     * @param weight
     * @param n
     * @param w
     * @return
     */
    public int knapsack(int[] weight, int n, int w) {
        // 构建二维二矩阵，n * (w+1)
        boolean[][] states = new boolean[n][w + 1];
        // 第一行特殊处理，利用哨兵优化
        // 第一行第一列，表示当前第一个物品不放入
        states[0][0] = true;
        // 第一行处理第一个物品
        if (weight[0] < w) {
            // 表示第一个物品放入
            states[0][weight[0]] = true;
        }

        // 从第二个物品开始
        for (int i = 1; i < n; i++) {
            for (int j = 0; j < w; j++) {
                // 第i个物品不放入包中（也就是从第二个物品不放入包中）
                if (states[i - 1][j] == true) {
                    states[i][j] = states[i - 1][j];
                }
            }

            // 放入背包中
            for (int j = 0; j <= w - weight[i]; j++) {
                // [i-1][j]==true表示遍历上一个物品放置到背包中的质量
                // 【i-1, j】调用了前一层计算到的最大值，每一层就算的都是重量上限为【0, j】的背包是否被满足
                if (states[i - 1][j] == true) {
                    states[i][j + weight[i]] = true;
                }
            }
        }

        for (int i = w; i >= 0; i--) {
            if (states[n - 1][i] == true) {
                return i;
            }
        }
        return 0;
    }

    public int knapsackTwo(int[] items, int n, int w) {
        // states数组里放置
        boolean[] states = new boolean[w + 1];
        states[0] = true;
        if (items[0] < w) {
            states[items[0]] = true;
        }

        for (int i = 1; i < n; i++) {
            for (int j = w - items[i]; j >= 0; j--) {
                if (states[j] == true) {
                    states[j + items[i]] = true;
                }
            }
        }

        for (int i = w; i >= 0; --i) { //
            // 输出结果
            if (states[i] == true) return i;
        }

        return 0;
    }

    public static int knapsackThree(int[] weight, int[] value, int n, int w) {
        /**
         * 一个二维数组 states[n][w+1]，来记录每层可以达到的不同状态。
         *
         * 不过这里数组存储的值不再是 boolean 类型的了，而是当前状态对应的最大总价值。
         *
         * 我们把每一层中 (i, cw) 重复的状态（节点）合并，只记录 cv 值最大的那个状态，然后基于这些状态来推导下一层的状态。
         */
        int[][] states = new int[n][w + 1];
        // 初始化states
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < w + 1; j++) {
                states[i][j] = -1;
            }
        }
        // 初始化二维数组第一个元素最大价值为0
        states[0][0] = 0;

        if (weight[0] <= w) {
            // 第一个物品放入背包中的价值
            states[0][weight[0]] = value[0];
        }

        for (int i = 1; i < n; i++) {
            // 第i个物品不放入背包
            for (int j = 0; j <= w; j++) {
                if (states[i - 1][j] >= 0) {
                    states[i][j] = states[i - 1][j];
                }
            }

            // 第i个物品放入背包
            for (int j = 0; j <= w - weight[i]; j++) {
                if (states[i - 1][j] >= 0) {
                    // v表示是假如把i物品放进去，将会产生的价值
                    int v = states[i - 1][j] + value[i];
                    // 若把物品i放进去后的总价值v，比当前重量j+物品i的重量所在位置价值高，替换它
                    if (v > states[i][j + weight[i]]) {
                        // 物品i放入后，该j+weight[i]重量的总价值使用v的
                        states[i][j + weight[i]] = v;
                    }
                }
            }
        }
        int maxValue = -1;
        for (int j = 0; j <= w; j++) {
            if (states[n - 1][j] > maxValue) {
                maxValue = states[n - 1][j];
            }
        }
        return maxValue;
    }

}
