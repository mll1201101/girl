package com.road.LeeCode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: girl
 * @description: 542. 01矩阵
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-01-11 18:18
 **/
public class UpdateMatrix {

    /**
     * 广度优先搜索
     *
     * @param matrix
     * @return
     */
    public int[][] updateMatrix(int[][] matrix) {
        if (matrix == null || matrix.length < 1 || matrix[0].length < 1) {
            return matrix;
        }
        // 上下左右四个方向
        int[][] dirs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

        // 矩阵长宽数
        int m = matrix.length, n = matrix[0].length;
        // new新二维数据（new新矩阵）
        int[][] dist = new int[m][n];
        // 表示矩阵中的元素是否被访问过
        boolean[][] seen = new boolean[m][n];
        // 将所有0元素位置添加到初始化队列中
        Queue<int[]> queue = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    queue.offer(new int[]{i, j});
                    seen[i][j] = true;
                }
            }
        }

        // 广度优先搜索
        while (!queue.isEmpty()) {
            int[] cell = queue.poll();
            // 取出0元素在矩阵中位置
            int i = cell[0], j = cell[1];
            for (int d = 0; d < 4; d++) {
                // 遍历0元素上下左右对应的元素
                int ni = i + dirs[d][0];
                int nj = j + dirs[d][1];
                //
                if (ni >= 0 && ni < m && nj >= 0 && nj < n && !seen[ni][nj]) {
                    dist[ni][nj] = dist[i][j] + 1;
                    queue.offer(new int[]{ni, nj});
                    seen[ni][nj] = true;
                }
            }
        }

        return dist;
    }

    public int orangeRotting(int[][] grid) {
        if (grid == null || grid.length < 1 || grid[0].length < 1) {
            return -1;
        }

        // 上下左右四个方向
        int[][] dirs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};

        // 矩阵长宽数
        int m = grid.length, n = grid[0].length;

        // 获取元素为1的个数
        int oneSum = 0;

        // 表示矩阵中的元素是否被访问过
        boolean[][] seen = new boolean[m][n];

        // 将所有1元素位置添加到初始化队列中
        Queue<int[]> queue = new LinkedList<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // 将所有2元素位置添加到初始化队列中
                if (grid[i][j] == 2) {
                    queue.offer(new int[]{i, j});
                }
                // 将所有1的元素
                if (grid[i][j] == 1) {
                    oneSum = oneSum + 1;
                }
            }
        }

        // 若无1的元素，直接返回
        if (oneSum < 1 || queue.isEmpty()) {
            return -1;
        }

        while (!queue.isEmpty()) {
            int[] cell = queue.poll();
            // 取出2元素在矩阵中位置
            int i = cell[0], j = cell[1];
            for (int d = 0; d < 4; d++) {
                // 遍历2元素上下左右对应的元素
                int ni = i + dirs[d][0];
                int nj = j + dirs[d][1];
                //
                if (ni >= 0 && ni < m && nj >= 0 && nj < n && !seen[ni][nj]) {
                    grid[ni][nj] = grid[i][j] + 1;
                    queue.offer(new int[]{ni, nj});
                    seen[ni][nj] = true;
                }
            }
        }

        return 0;
    }
}
