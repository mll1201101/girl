package com.road.LeeCode;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: girl
 * @description: 695.岛屿最大面积
 *
 * 给你一个大小为 m x n 的二进制矩阵 grid 。
 *
 * 岛屿 是由一些相邻的 1 (代表土地) 构成的组合，这里的「相邻」要求两个 1 必须在 水平或者竖直的四个方向上 相邻。
 * 你可以假设 grid 的四个边缘都被 0（代表水）包围着。
 *
 * 岛屿的面积是岛上值为 1 的单元格的数目。
 *
 * 计算并返回 grid 中最大的岛屿面积。如果没有岛屿，则返回面积为 0 。
 *
 * <p>
 * 你可以把每个1都想象成一块面积为1的正方形陆地，每个0都是一块面积为1的正方形水域。
 * 因为正方形有上下左右四个边，只要两块陆地在上下左右这四个方向之一是相连的，它们就可以组成一块更大的岛屿。
 * 举几个例子：
 * <p>
 * 例一：
 * [[1,1,0],
 * [1,0,0],
 * [0,0,1]]
 * 上述网格中，左上角有一块面积为3的岛屿，右下角有一个面积为1的岛屿，所以我们要返回较大的3.
 * <p>
 * 例二：
 * [[1,0],
 * [0,1]]
 * 上述网格中，两小块陆地看起来离得也挺近，好像连起来了，但其实没有连上。
 * 因为只是两个正方形的角对上了，边没碰上，不符合上下左右四个方向中有一个方向相连。
 * 所以这两块小陆地都是独立的小岛，返回答案1.
 * <p>
 * 例三：
 * [[1,1,1],
 * [1,0,1],
 * [1,1,1]]
 * 上述网格比较特殊。看起来中间还有水域，但其实水域并没有把周围的陆地彻底隔开。
 * 我们仔细观察，每块陆地都与相邻的陆地在某个方向上相连。
 * 所以这是一整个大岛屿，应该返回答案8，中间的水域是岛屿内部的水域。
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-01-06 10:43
 **/
public class MaxAreaOfIsland {
    /**
     * 广度优先搜索
     *
     * @param grid
     * @return
     */
    public int maxAreaOfIsland(int[][] grid) {
        int ans = 0;
        for (int i = 0; i != grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                int cur = 0;
                Queue<Integer> queuei = new LinkedList<>();
                Queue<Integer> queuej = new LinkedList<>();
                queuei.offer(i);
                queuej.offer(j);
                while (!queuei.isEmpty()) {
                    int cur_i = queuei.poll(), cur_j = queuej.poll();
                    if (cur_i < 0 || cur_j < 0 || cur_i == grid.length || cur_j == grid[0].length || grid[cur_i][cur_j] != 1) {
                        continue;
                    }
                    ++cur;
                    grid[cur_i][cur_j] = 0;
                    int[] di = {0, 0, 1, -1};
                    int[] dj = {1, -1, 0, 0};
                    for (int index = 0; index != 4; ++index) {
                        int next_i = cur_i + di[index], next_j = cur_j + dj[index];
                        queuei.offer(next_i);
                        queuej.offer(next_j);
                    }
                }
                ans = Math.max(ans, cur);
            }
        }
        return ans;
    }

    /**
     * 深度优先搜素
     *
     * @param grid
     * @return
     */
    public int maxAreaOfIslandDfs(int[][] grid) {
        int ans = 0;
        for (int i = 0; i != grid.length; ++i) {
            for (int j = 0; j < grid[0].length; ++j) {
                ans = Math.max(ans, dfs(grid, i, j));
            }
        }
        return ans;
    }

    public int dfs(int[][] grid, int cur_i, int cur_j) {
        if (cur_i < 0 || cur_j < 0 || cur_i == grid.length || cur_j == grid[0].length || grid[cur_i][cur_j] != 1) {
            return 0;
        }
        grid[cur_i][cur_j] = 0;
        int[] di = {0, 0, 1, -1};
        int[] dj = {1, -1, 0, 0};
        int ans = 1;
        for (int index = 0; index != 4; index++) {
            /**
             * // up,当前节点减一，就是行数减一，回到当前元素的上行元素
             * 	travel(grid, i-1, j)
             * 	// left
             * 	travel(grid, i, j-1)
             * 	// right
             * 	travel(grid, i, j+1)
             * 	// down
             * 	travel(grid, i+1, j)
             */
            int next_i = cur_i + di[index], next_j = cur_j + dj[index];
            ans += dfs(grid, next_i, next_j);
        }
        return ans;
    }

    /**
     *
     * 方法二：深度优先搜索 + 栈
     * 算法
     *
     * 我们可以用栈来实现深度优先搜索算法。这种方法本质与方法一相同，唯一的区别是：
     *
     * 方法一通过函数的调用来表示接下来想要遍历哪些土地，让下一层函数来访问这些土地。而方法二把接下来想要遍历的土地放在栈里，然后在取出这些土地的时候访问它们。
     *
     * 访问每一片土地时，我们将对围绕它四个方向进行探索，找到还未访问的土地，加入到栈 stack 中；
     *
     * 另外，只要栈 stack 不为空，就说明我们还有土地待访问，那么就从栈中取出一个元素并访问。
     *
     *
     * 复杂度分析
     *
     * 时间复杂度：O(R \times C)O(R×C)。其中 RR 是给定网格中的行数，CC 是列数。我们访问每个网格最多一次。
     *
     * 空间复杂度：O(R \times C)O(R×C)，栈中最多会存放所有的土地，土地的数量最多为 R \times CR×C 块，因此使用的空间为 O(R \times C)O(R×C)。
     *
     * @param grid
     * @return
     */
    public int maxAreaOfIslandStack(int[][] grid) {
        int ans = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j != grid[0].length; j++) {
                int cur = 0;
                Deque<Integer> stacki = new LinkedList<>();
                Deque<Integer> stackj = new LinkedList<>();
                stacki.push(i);
                stackj.push(j);
                while (!stacki.isEmpty()) {
                    int cur_i = stacki.pop(), cur_j = stackj.pop();
                    if (cur_i < 0 || cur_j < 0 || cur_i == grid.length || cur_j == grid[0].length || grid[i][j] != 1) {
                        continue;
                    }
                    ++cur;
                    grid[cur_i][cur_j] = 0;
                    int[] di = {0, 0, 1, -1};
                    int[] dj = {1, -1, 0, 0};
                    for (int index = 0; index != 4; ++index) {
                        int next_i = cur_i + di[index], next_j = cur_j + dj[index];
                        stacki.push(next_i);
                        stackj.push(next_j);
                    }
                }
                ans = Math.max(ans, cur);
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        int[][] grid = new int[][]{
                {0,0,1,0,0,0,0,1,0,0,0,0,0},
                {0,0,0,0,0,0,0,1,1,1,0,0,0},
                {0,1,1,0,1,0,0,0,0,0,0,0,0},
                {0,1,0,0,1,1,0,0,1,0,1,0,0},
                {0,1,0,0,1,1,0,0,1,1,1,0,0},
                {0,0,0,0,0,0,0,0,0,0,1,0,0},
                {0,0,0,0,0,0,0,1,1,1,0,0,0},
                {0,0,0,0,0,0,0,1,1,0,0,0,0}};
        System.out.println("grid length:" + grid.length);
        int result = new MaxAreaOfIsland().maxAreaOfIslandDfs(grid);
        System.out.println("grid max area:" + result);
    }
}
