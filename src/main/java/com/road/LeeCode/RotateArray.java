package com.road.LeeCode;

import java.util.Arrays;

/**
 * @program: girl
 * @description: 轮转数据
 *
 * 给你一个数组，将数组中的元素向右轮转 k 个位置，其中 k 是非负数
 *
 * 示例 1:
 *
 * 输入: nums = [1,2,3,4,5,6,7], k = 3
 * 输出: [5,6,7,1,2,3,4]
 * 解释:
 * 向右轮转 1 步: [7,1,2,3,4,5,6]
 * 向右轮转 2 步: [6,7,1,2,3,4,5]
 * 向右轮转 3 步: [5,6,7,1,2,3,4]
 *
 * 示例2:
 * 输入：nums = [-1,-100,3,99], k = 2
 * 输出：[3,99,-1,-100]
 * 解释:
 * 向右轮转 1 步: [99,-1,-100,3]
 * 向右轮转 2 步: [3,99,-1,-100]
 *
 * 提示：
 * 1 <= nums.length <= 105
 * -231 <= nums[i] <= 231 - 1
 * 0 <= k <= 105
 *
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2021-12-23 15:31
 **/
public class RotateArray {

    /**
     * 使用双指针
     *
     * @param nums
     * @param k
     */
    public static void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 0) {
            return;
        }

        // 定义双指针一左一右
        int left = 0, right = nums.length - 1, length = nums.length;

        int[] res = new int[nums.length];

        // 左指针小于右指针执行以下逻辑
        while (left <= right) {
            // 右指针操作
            // 将原数组下标为i的元素放至新数组下标为 (i+k) % n 的位置，最后将新数组拷贝至原数组即可
            res[(right + k) % length] = nums[right];
            right--;

            // 左指针操作
            res[(left + k) % length] = nums[left];
            left++;
        }
        System.arraycopy(res, 0, nums, 0, length);
        System.out.println("res" + Arrays.toString(res));
    }

    public static void main(String[] args) {
        int[] intArray = {1,2,3,4,5,6,7};
        rotate(intArray, 10);
        int k = 3, length = 7;
        System.out.println(k %= length);
        System.out.println(k = k % length);
    }
}
