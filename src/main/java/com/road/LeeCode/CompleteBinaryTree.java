package com.road.LeeCode;

import com.road.LeeCode.treenode.BinaryTree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @program: girl
 * @description: 116.填充每个节点的下个节点的右侧指针
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-01-11 14:15
 **/
public class CompleteBinaryTree {
    public BinaryTree connect(BinaryTree root) {
        if (root == null) {
            return null;
        }
        // 初始化队列同时将第一层节点加入队列中，即根节点
        Queue<BinaryTree> queue = new LinkedList<>();
        queue.add(root);

        // 外层while循环迭代的是层数
        while (!queue.isEmpty()) {
            int size = queue.size();

            for (int i = 0; i < size; i++) {
                BinaryTree node = queue.poll();
                if (i < size - 1) {
                    node.next = queue.peek();
                }

                if (node.left != null) {
                    queue.add(node.left);
                }

                if (node.right != null) {
                    queue.add(node.right);
                }
            }
        }
        return root;
    }

    public BinaryTree connectBfs(BinaryTree root) {
        if (root == null || root.left == null) {
            return root;
        }

        root.left.next = root.right;
        // 本判断，保证递归调用中，二叉树是完全二叉树，并且保证不同根节点的右节点可以指向不同节点的左节点
        if (root.next != null && root.right != null && root.next.right != null) {
            root.right.next = root.next.left;
        }
        connectBfs(root.left);
        connectBfs(root.right);
        return root;
    }
}
