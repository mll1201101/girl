package com.road.LeeCode;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: girl
 * @description: 无重复字符的最长子串
 * 给定一个字符串 s ，请你找出其中不含有重复字符的 最长子串 的长度。
 *
 * 示例 1:
 * 输入: s = "abcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 *
 * 示例 2:
 * 输入: s = "bbbbb"
 * 输出: 1
 * 解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
 *
 * 示例 3:
 * 输入: s = "pwwkew"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
 * 请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
 *
 * 示例 4:
 * 输入: s = ""
 * 输出: 0
 *
 * 提示：
 * 0 <= s.length <= 5 * 104
 * s 由英文字母、数字、符号和空格组成
 *
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2021-12-27 15:45
 **/
public class LengthOfLongestSubstring {
    /**
     * 滑动窗口
     *
     * @param s
     * @return
     */
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0)
            return 0;
        // 哈希集合，记录每个字符是否出现过
        Set<Character> occ = new HashSet<Character>();
        // 字符串长度
        int n = s.length();
        // 右指针初始值为-1，相当于我们在字符串的左边界左侧，未开始移动
        int rightP = -1, ans = 0;
        for (int i = 0; i < n; i++) {
            if (i != 0) {
                // 左指针向右移动一格，移除一格字符
                occ.remove(s.charAt(i - 1));
            }
            while (rightP + 1 < n && !occ.contains(s.charAt(rightP + 1))) {
                // 不断移动右指针
                occ.add(s.charAt(rightP + 1));
                ++rightP;
            }
            // 第i到rightP个字符是一个极长的无重复字符子串
            ans = Math.max(ans, rightP - i + 1);
        }
        return ans;
    }
}
