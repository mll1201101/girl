package com.road.LeeCode;

/**
 * @program: girl
 * @description: 283. 移动零
 *
 * 给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
 *
 * 示例:
 * 输入: [0,1,0,3,12]
 * 输出: [1,3,12,0,0]
 *
 * 说明:
 * 必须在原数组上操作，不能拷贝额外的数组。
 * 尽量减少操作次数。
 *
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2021-12-23 16:56
 **/
public class MoveZeroes {

    /**
     * 使用双指针，原地使用数组
     * @param nums
     */
    public void moveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }

        int n = nums.length, left = 0, right = 0;
        // 1、左指针左边均为非零数
        // 2、右指针左边直到left左指针均为零
        while (right < n) {
            System.out.println("left==" + left);
            System.out.println("right==" + right);
            if (nums[right] != 0) {
                System.out.println("nums[left]==" + nums[left]);
                System.out.println("nums[right]==" + nums[right]);
                swap(nums, left, right);
                left++;
            }
            right++;
        }
    }

    public void swap(int[] nums, int left, int right) {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{0,0,0,3,12};
        new MoveZeroes().moveZeroes(nums);
    }
}
