package com.road.LeeCode;

/**
 * @program: girl
 * @description:
 * @packagename: com.road.LeeCode
 * @author: Road
 * @date: 2022-02-09 17:38
 **/
public class CanJump {

    public int maxW = Integer.MIN_VALUE; //存储背包中物品总重量的最大值

    // cw表示当前已经装进去的物品的重量和；
    // i表示考察到哪个物品了；
    // w背包重量；
    // items表示每个物品的重量；
    // n表示物品个数
    // 假设背包可承受重量100，物品个数10，物品重量存储在数组a中，那可以这样调用函数：
    // f(0, 0, a, 10, 100)
    public void f(int i, int cw, int[] items, int n, int w) {
        if (cw == w || i == n) { // cw==w表示装满了;i==n表示已经考察完所有的物品
            if (cw > maxW) maxW = cw;
            return;
        }
        //当前物品不装进背包，直接遍历下个物品
        f(i + 1, cw, items, n, w);
        if (cw + items[i] <= w) {// 已经超过可以背包承受的重量的时候，就不要再装了
            f(i + 1, cw + items[i], items, n, w);
        }
    }

    public boolean canJump(int[] nums) {
        int canReachMaxIndex = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == 0 && canReachMaxIndex == i) {
                return false;
            }
            canReachMaxIndex = Math.max(canReachMaxIndex, nums[i]);
            if (canReachMaxIndex >= nums.length - 1) {
                return true;
            }
        }
        return false;
    }

    public boolean canJumpTwo(int[] nums) {
        boolean[] dp = new boolean[nums.length];
        dp[0] = true;

        for (int i = 1; i < nums.length; i++) {
            for (int j = 0; j < i; j++) {
                if (dp[j] && nums[j] + j >= i) {
                    dp[i] = true;
                    break;
                }
            }
        }
        return dp[nums.length - 1];
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 1, 0, 4};
        new CanJump().canJump(nums);
    }
}
