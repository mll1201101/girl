package com.road.LeeCode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 217. 存在重复元素
 *
 * 给定一个整数数组，判断是否存在重复元素。
 *
 * 如果存在一值在数组中出现至少两次，函数返回 true 。如果数组中每个元素都不相同，则返回 false
 *
 * 示例 1:
 *
 * 输入: [1,2,3,1]
 * 输出: true
 * 示例 2:
 *
 * 输入: [1,2,3,4]
 * 输出: false
 * 示例 3:
 *
 * 输入: [1,1,1,3,3,4,3,2,4,2]
 * 输出: true
 */
public class ContainsDuplication {
    /**
     * 哈希方式解决
     *
     * @param nums
     * @return
     */
    public boolean hashContainsDuplication(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>(nums.length);
        boolean result = false;
        for (int i = 0; i < nums.length; i++) {
            if (!map.containsKey(nums[i])) {
                map.put(nums[i], nums[i]);
            } else {
                result =  true;
            }
        }
        return result;
    }

    /**
     * 排序
     *
     * 我们可以扫描已排序的数组，每次判断相邻的两个元素是否相等，如果相等则说明存在重复的元素
     * @param nums
     * @return
     */
    public boolean containsDuplication(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length;
        for (int i = 0; i < n - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return true;
            }
        }
        return false;
    }
}
