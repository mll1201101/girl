package com.road.lambda;

import java.util.Arrays;
import java.util.List;

/**
 * @author malulu
 * @desc
 * @date 2020/10/19
 */
public class JavaStreamExample {
    public static void main(String[] args) {
        List<String> stringList = Arrays.asList("abc", "", "bc", "efg", "abcd","", "jkl");
        // 获取空字符串的数量
        long count = stringList.parallelStream().filter(string -> string.isEmpty()).count();
    }
}
