package com.vision.girl.user.mapper;

import com.vision.girl.user.entity.BdPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface BdPermissionMapper extends BaseMapper<BdPermission> {

}
