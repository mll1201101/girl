package com.vision.girl.user.mapper;

import com.vision.girl.user.entity.BdModule;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 功能模块表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface BdModuleMapper extends BaseMapper<BdModule> {

}
