package com.vision.girl.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.vision.girl.user.entity.BdRole;


/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface BdRoleMapper extends BaseMapper<BdRole> {

}
