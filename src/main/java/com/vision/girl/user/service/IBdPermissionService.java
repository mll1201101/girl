package com.vision.girl.user.service;

import com.vision.girl.user.entity.BdPermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface IBdPermissionService extends IService<BdPermission> {

}
