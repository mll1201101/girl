package com.vision.girl.user.service.impl;

import com.vision.girl.user.entity.BdPermission;
import com.vision.girl.user.mapper.BdPermissionMapper;
import com.vision.girl.user.service.IBdPermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@Service
public class BdPermissionServiceImpl extends ServiceImpl<BdPermissionMapper, BdPermission> implements IBdPermissionService {

}
