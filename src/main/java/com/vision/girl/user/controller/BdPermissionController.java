package com.vision.girl.user.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.vision.girl.common.BaseController;

/**
 * <p>
 * 权限表 前端控制器
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@RestController
@RequestMapping("/user/bd-permission")
public class BdPermissionController extends BaseController {

}
