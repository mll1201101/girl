package com.vision.girl.meet.controller;

import com.vision.girl.common.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Jack Road
 * @since 2019-09-23
 */
@RestController
@RequestMapping("/meet/re-user-meet")
public class ReUserMeetController extends BaseController {

}

