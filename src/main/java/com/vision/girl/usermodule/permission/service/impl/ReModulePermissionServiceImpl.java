package com.vision.girl.usermodule.permission.service.impl;

import com.vision.girl.usermodule.permission.entity.ReModulePermission;
import com.vision.girl.usermodule.permission.mapper.ReModulePermissionMapper;
import com.vision.girl.usermodule.permission.service.IReModulePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 模块与权限关系表 服务实现类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@Service
public class ReModulePermissionServiceImpl extends ServiceImpl<ReModulePermissionMapper, ReModulePermission> implements IReModulePermissionService {

}
