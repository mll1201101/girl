package com.vision.girl.usermodule.permission.service;

import com.vision.girl.usermodule.permission.entity.ReModulePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 模块与权限关系表 服务类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface IReModulePermissionService extends IService<ReModulePermission> {

}
