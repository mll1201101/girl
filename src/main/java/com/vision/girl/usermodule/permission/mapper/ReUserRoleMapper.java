package com.vision.girl.usermodule.permission.mapper;

import com.vision.girl.user.entity.ReUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户与角色关系表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface ReUserRoleMapper extends BaseMapper<ReUserRole> {

}
