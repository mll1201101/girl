package com.vision.girl.usermodule.permission.service.impl;

import com.vision.girl.user.entity.ReUserRole;
import com.vision.girl.usermodule.permission.mapper.ReUserRoleMapper;
import com.vision.girl.usermodule.permission.service.IReUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户与角色关系表 服务实现类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@Service
public class ReUserRoleServiceImpl extends ServiceImpl<ReUserRoleMapper, ReUserRole> implements IReUserRoleService {

}
