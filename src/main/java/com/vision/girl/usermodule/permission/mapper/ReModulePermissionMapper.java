package com.vision.girl.usermodule.permission.mapper;

import com.vision.girl.usermodule.permission.entity.ReModulePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模块与权限关系表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface ReModulePermissionMapper extends BaseMapper<ReModulePermission> {

}
