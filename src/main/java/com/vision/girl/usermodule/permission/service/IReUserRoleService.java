package com.vision.girl.usermodule.permission.service;

import com.vision.girl.user.entity.ReUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户与角色关系表 服务类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface IReUserRoleService extends IService<ReUserRole> {

}
