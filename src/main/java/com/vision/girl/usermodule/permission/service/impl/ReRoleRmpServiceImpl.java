package com.vision.girl.usermodule.permission.service.impl;

import com.vision.girl.usermodule.permission.entity.ReRoleRmp;
import com.vision.girl.usermodule.permission.mapper.ReRoleRmpMapper;
import com.vision.girl.usermodule.permission.service.IReRoleRmpService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色与re_module_permission关系表 服务实现类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@Service
public class ReRoleRmpServiceImpl extends ServiceImpl<ReRoleRmpMapper, ReRoleRmp> implements IReRoleRmpService {

}
