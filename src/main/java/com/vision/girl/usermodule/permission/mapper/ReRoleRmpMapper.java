package com.vision.girl.usermodule.permission.mapper;

import com.vision.girl.usermodule.permission.entity.ReRoleRmp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色与re_module_permission关系表 Mapper 接口
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface ReRoleRmpMapper extends BaseMapper<ReRoleRmp> {

}
