package com.vision.girl.usermodule.permission.service;

import com.vision.girl.usermodule.permission.entity.ReRoleRmp;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色与re_module_permission关系表 服务类
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
public interface IReRoleRmpService extends IService<ReRoleRmp> {

}
