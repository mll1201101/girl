package com.vision.girl.usermodule.permission.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;
import com.vision.girl.common.BaseController;

/**
 * <p>
 * 模块与权限关系表 前端控制器
 * </p>
 *
 * @author Jack Road
 * @since 2020-04-11
 */
@RestController
@RequestMapping("/usermodule.permission/re-module-permission")
public class ReModulePermissionController extends BaseController {

}
