FROM tomcat:9.0.34
MAINTAINER Jack Road
ADD ./target/girl-0.0.1-SNAPSHOT.jar /usr/local/tomcat/webapps/
CMD ["catalina.sh","run"]
